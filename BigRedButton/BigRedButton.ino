/*
  BigRedButton
  A stateful big red button driving thing.

  Requires Bounce library:
  https://github.com/thomasfredericks/Bounce-Arduino-Wiring
*/
#include "Bounce.h"

static const int soundSwitch = 0; // Switch to control whether to play sound.
static const int mainSwitch = 1; // Main locking switch.
static const int safetySwitch = 2; // Safety toggle switch.

static const int redLED = 8; // Condition Red LED.
static const int yellowLED = 9; // Condition Yellow LED.
static const int greenLED = 10; // Condition Green LED.

static const int piezoPin = 11; // Pin that drives the piezo buzzer.

typedef enum {
  CONDITION_RED,
  CONDITION_YELLOW,
  CONDITION_GREEN} state;
state currentState, previousState;

int mainSwitchState, safetySwitchState;
Bounce safetySwitchBouncer = Bounce();
Bounce mainSwitchBouncer = Bounce();

void noise() {
  unsigned long time = millis();
  while(millis() - time <= 2000) {
    if (digitalRead(soundSwitch) == LOW) {
      tone(piezoPin, random(100, 1000));
    } else {
      noTone(piezoPin);
    }
  }
  noTone(piezoPin);
}

void conditionGreen() {
  previousState = currentState;
  currentState = CONDITION_GREEN;
  digitalWrite(greenLED, HIGH);
  digitalWrite(yellowLED, LOW);
  digitalWrite(redLED, LOW);
}

void conditionYellow() {
  previousState = currentState;
  currentState = CONDITION_YELLOW;
  digitalWrite(greenLED, LOW);
  digitalWrite(yellowLED, HIGH);
  digitalWrite(redLED, LOW);
}

void conditionRed() {
  previousState = currentState;
  currentState = CONDITION_RED;
  digitalWrite(greenLED, LOW);
  digitalWrite(yellowLED, LOW);
  digitalWrite(redLED, HIGH);
}


void mainSwitchToggled(int newState) {
  if (currentState == CONDITION_YELLOW && newState == LOW) {
    conditionRed();
    Keyboard.println();
    noise();
  } else if (currentState == CONDITION_RED && newState == HIGH) {
    conditionYellow();
  }
  mainSwitchState = newState;
}

void safetySwitchToggled(int newState) {
  if (newState == LOW && mainSwitchState == HIGH) {
    conditionYellow();
  } else if (newState == HIGH) {
    conditionGreen();
  }
  safetySwitchState = newState;
}

void setup() {
  pinMode(redLED, OUTPUT);
  pinMode(yellowLED, OUTPUT);
  pinMode(greenLED, OUTPUT);

  // funky init stuff. I want a delay before Keyboard starts, just in case.
  digitalWrite(redLED, HIGH);
  digitalWrite(yellowLED, HIGH);
  digitalWrite(greenLED, HIGH);
  int pins[] = {greenLED, yellowLED, redLED};
  for (int i=0; i<5000; i+=150) {
    uint8_t pin = pins[(i/150)%3];
    digitalWrite(pin, LOW);
    delay(150);
    digitalWrite(pin, HIGH);
  }
  tone(piezoPin, 3000, 150);
  delay(150);
  tone(piezoPin, 3000, 150);
  randomSeed(analogRead(0));
  pinMode(soundSwitch, INPUT);
  pinMode(mainSwitch, INPUT);
  pinMode(safetySwitch, INPUT);
  digitalWrite(soundSwitch, HIGH);
  digitalWrite(mainSwitch, HIGH);
  digitalWrite(safetySwitch, HIGH);
  mainSwitchBouncer.attach(mainSwitch);
  safetySwitchBouncer.attach(safetySwitch);
  mainSwitchBouncer.interval(5);
  safetySwitchBouncer.interval(5);

  pinMode(piezoPin, OUTPUT);

  conditionGreen();
  mainSwitchState = digitalRead(mainSwitch);
  safetySwitchState = digitalRead(safetySwitch);

  Keyboard.begin();
}

void loop() {
  mainSwitchBouncer.update();
  safetySwitchBouncer.update();

  int curMainState = mainSwitchBouncer.read();
  if (curMainState != mainSwitchState) {
    mainSwitchToggled(curMainState);
  }
  int curSafetyState = safetySwitchBouncer.read();
  if (curSafetyState != safetySwitchState) {
    safetySwitchToggled(curSafetyState);
  }
}
