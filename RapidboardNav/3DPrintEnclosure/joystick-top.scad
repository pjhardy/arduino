render = false;

if (render) {
     $fs = 0.01; // 0.01 for high detail
} else {
     $fs = 0.3;
}
final_rotation = render ? [0, 180, 0] : [0, 0, 0];

use <boxes.scad>;

large_cube_size = [90, 110, 50];
wall_thickness = 3;
cube_radius = 10;
small_cube_size = [(large_cube_size[0]-2*wall_thickness),
                   (large_cube_size[1]-2*wall_thickness),
                   (large_cube_size[2]-2*wall_thickness)];
button_enc_radius = 20;
button_hole_radius = 15.5;

left_button_location = [-(large_cube_size[0]/2 - button_enc_radius),
                        (large_cube_size[1]/2 - cube_radius),
                        -(large_cube_size[2]/2)];
right_button_location = [(large_cube_size[0]/2 - button_enc_radius),
                         (large_cube_size[1]/2 - cube_radius),
                         -(large_cube_size[2]/2)];
rotate(final_rotation) {
     difference() {
          // main body
          union() {
               roundedBox(large_cube_size, cube_radius);
               translate(left_button_location) {
                    cylinder(r=button_enc_radius, h=large_cube_size[2]);
               }
               translate(right_button_location) {
                    cylinder(r=button_enc_radius, h=large_cube_size[2]);
               }
          }
          // hollow it
          roundedBox(small_cube_size, cube_radius);
          translate(left_button_location) {
               cylinder(r=button_enc_radius-wall_thickness,
                        h=large_cube_size[2]-wall_thickness);
          }
          translate(right_button_location) {
               cylinder(r=button_enc_radius-wall_thickness,
                        h=large_cube_size[2]-wall_thickness);
          }
          // Cut off the bottom
          translate([0, 0, cube_radius/2-(large_cube_size[2]/2)-1]) {
               cube([large_cube_size[0]+2,
                     large_cube_size[1]+(2*button_enc_radius)+2,
                     cube_radius+2], center=true);
          }
          // bottom lip
          translate([0, 0, -large_cube_size[2]/2]) {
               roundedBox([large_cube_size[0]-wall_thickness+0.2,
                           large_cube_size[1]-wall_thickness+0.2,
                           2*(cube_radius+5)], cube_radius, sidesonly=true);
          }
          translate(left_button_location) {
               cylinder(r=(button_enc_radius-(wall_thickness/2)+0.2),
                        h=(cube_radius+5));
          }
          translate(right_button_location) {
               cylinder(r=(button_enc_radius-(wall_thickness/2)+0.2),
                        h=(cube_radius+5));
          }
          // joystick mounting holes
          joystickholes(position=[0, -10, (large_cube_size[2]/2)],
                        thickness=wall_thickness);
          // button mounting holes
          translate(left_button_location) {
               cylinder(r=button_hole_radius,
                        h=large_cube_size[2]+1);
          }
          translate(right_button_location) {
               cylinder(r=button_hole_radius,
                        h=large_cube_size[2]+1);
          }
                        
     }
}

module joystickholes(position = [0, 0, 0], thickness = 1) {
    centre_radius = 25/2;
    mounting_radius = 2.1;
    cylinder_height = thickness + 0.2;
    mounting_hole_x = 50;
    mounting_hole_y = 50;

    x = position[0];
    y = position[1];
    z = position[2] - thickness - 0.1;

    translate([x, y, z]) {
        cylinder(r=centre_radius, h=cylinder_height);
        translate([-(mounting_hole_x/2), -(mounting_hole_y/2), 0]) {
            cylinder(r=mounting_radius, h=cylinder_height);
        }
        translate([(mounting_hole_x/2), -(mounting_hole_y/2), 0]) {
            cylinder(r=mounting_radius, h=cylinder_height);
        }
        translate([(mounting_hole_x/2), (mounting_hole_y/2), 0]) {
            cylinder(r=mounting_radius, h=cylinder_height);
        }
        translate([-(mounting_hole_x/2), (mounting_hole_y/2), 0]) {
            cylinder(r=mounting_radius, h=cylinder_height);
        }
    }
}
