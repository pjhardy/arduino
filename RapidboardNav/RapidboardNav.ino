/* RapidboardNav.ino
   Navigating a JIRA rapid board with Arduino.
   Uses an 8-way microswitch joystick, attached to pins 4-7, for navigating issues.
   Has four additional pushbutton switchs on pins 8-11 for opening issue details,
   and for rudimentary browser tab navigation.

   Dependencies:
   * Uses the Arduino Keyboard library (http://arduino.cc/en/Reference/MouseKeyboard),
   thus requires a Leonardo, Micro or Due Arduino-compatible board.
   * Uses the Bounce2 library (https://github.com/thomasfredericks/Bounce2).

   Peter Hardy <peter@hardy.dropbear.id.au>
*/
#include <Bounce2.h>

const byte mouseMode = 2;     // pin for mouse mode switch
const byte keyMode = 3;       // pin for keyboard mode switch
const byte joyUp = 7;         // pin for joystick up
const byte joyDown = 6;       // pin for joystick down
const byte joyLeft = 5;       // pin for joystick left
const byte joyRight = 4;      // pin for joystick right

const byte nextButton = 10;    // pin for next button
const byte prevButton = 8;    // pin for previous button
const byte reloadButton = 9; // pin for reload button
const byte detailButton = 11; // pin for detail button

const int debounceInterval = 5; // how often to sample button presses

// Joystick modes
enum mode {
  JIRA = 1,
  MOUSE = 2,
  ARROW = 4
};
enum mode joystickMode;

unsigned long lastMouse = 0; // When we last sent mouse data
const char mousePoll = 10; // How often to send mouse data in ms.
const char cursorSpeed = 20; // How far to move the pointer. Pixels I think?
const char scrollSpeed = 1; // How fast to scroll.
const char scrollFreq = 3; // Only send scroll info this many updates
int xMove, yMove, scrollMove; // Buffers for where to move the mouse.

// constants representing the different buttons
const byte JOYSTICK_UP = 0;
const byte JOYSTICK_DOWN = 1;
const byte JOYSTICK_LEFT = 2;
const byte JOYSTICK_RIGHT = 3;
const byte NEXT_BUT = 4;
const byte PREV_BUT = 5;
const byte RELOAD_BUT = 6;
const byte DETAIL_BUT = 7;
const byte MOUSE_BUT = 8;
const byte KEY_BUT = 9;

/* An array of Bounce objects to take care of
   software debouncing for all of our buttons.
                  __
              _ ."--". _
             (_:/ oo \:_)
         /\,   _(`()`)_   ,/\
         \ `~~~,(`""`),~~~` /
          `'---|/`""`\|---'`
               ||    ||
           _   ||    ||   _
          //)  ||    ||  (\\
         (/ '-.\\    //.-' \)
          `"=.,__`""`__,.-"`
                 }=-{
                 {==}
                 }-={
                 {==}
                 }-={
      jgs        {==}
                  ~~
*/
Bounce controls[10] = { Bounce(), Bounce(), Bounce(), Bounce(), Bounce(),
                        Bounce(), Bounce(), Bounce(), Bounce(), Bounce()};

void setup() {
  xMove = 0;
  yMove = 0;
  scrollMove = 0;

  pinMode(mouseMode, INPUT_PULLUP);
  pinMode(keyMode, INPUT_PULLUP);
  controls[MOUSE_BUT].attach(mouseMode);
  controls[KEY_BUT].attach(keyMode);

  pinMode(joyUp, INPUT_PULLUP);
  pinMode(joyDown, INPUT_PULLUP);
  pinMode(joyLeft, INPUT_PULLUP);
  pinMode(joyRight, INPUT_PULLUP);
  controls[JOYSTICK_UP].attach(joyUp);
  controls[JOYSTICK_DOWN].attach(joyDown);
  controls[JOYSTICK_LEFT].attach(joyLeft);
  controls[JOYSTICK_RIGHT].attach(joyRight);

  pinMode(nextButton, INPUT_PULLUP);
  pinMode(prevButton, INPUT_PULLUP);
  pinMode(reloadButton, INPUT_PULLUP);
  pinMode(detailButton, INPUT_PULLUP);
  controls[NEXT_BUT].attach(nextButton);
  controls[PREV_BUT].attach(prevButton);
  controls[RELOAD_BUT].attach(reloadButton);
  controls[DETAIL_BUT].attach(detailButton);

  // Delay to let pins settle, and ensure uploading
  // replacement sketches still works in an emergency.
  delay(5000);
  Keyboard.begin();
  Mouse.begin();
  Serial.begin(115200);
}

void loop() {
  // Update mode state
  controls[MOUSE_BUT].update();
  controls[KEY_BUT].update();

  
  if (controls[MOUSE_BUT].read() == LOW) {
    if (joystickMode != MOUSE) {
      Serial.println("Entering mouse mode");
      joystickMode = MOUSE;
    }
  } else if (controls[KEY_BUT].read() == LOW) {
    if (joystickMode != ARROW) {
      Serial.println("Entering arrow mode");
      joystickMode = ARROW;
    }
  } else {
    if (joystickMode != JIRA) {
      Serial.println("Entering JIRA mode");
      joystickMode = JIRA;
    }
  }

  for (int i=0; i<8; i++) {
    if (controls[i].update()) {
      if (controls[i].read() == HIGH) {
        switch(i) {
        case(JOYSTICK_UP):
          joyUpPressed();
          break;
        case(JOYSTICK_DOWN):
          joyDownPressed();
          break;
        case(JOYSTICK_LEFT):
          joyLeftPressed();
          break;
        case(JOYSTICK_RIGHT):
          joyRightPressed();
          break;
        case(NEXT_BUT):
          nextReleased();
          break;
        case(PREV_BUT):
          prevReleased();
          break;
        case(DETAIL_BUT):
          detailReleased();
          break;
        case(RELOAD_BUT):
          reloadReleased();
          break;
        }
      } else {
        switch(i) {
        case(JOYSTICK_UP):
          joyUpReleased();
          break;
        case(JOYSTICK_DOWN):
          joyDownReleased();
          break;
        case(JOYSTICK_LEFT):
          joyLeftReleased();
          break;
        case(JOYSTICK_RIGHT):
          joyRightReleased();
          break;
        case(NEXT_BUT):
          nextPressed();
          break;
        case(PREV_BUT):
          prevPressed();
          break;
        case(DETAIL_BUT):
          detailPressed();
          break;
        case(RELOAD_BUT):
          reloadPressed();
          break;
        }
      }
    }
  }

  if (joystickMode == MOUSE) {
    unsigned long now = millis();
    if (lastMouse + mousePoll < now) {
      Mouse.move(xMove, yMove, scrollMove);
      lastMouse = now;
    }
  }
}

void joyLeftPressed() {
  Serial.println("joy left pressed");
  switch(joystickMode) {
  case JIRA:
    Keyboard.press('p'); // JIRA rapidboard previous column
    break;
  case MOUSE:
    xMove = -cursorSpeed;
    break;
  case ARROW:
    Keyboard.press(KEY_LEFT_ARROW);
    break;
  }
}

void joyLeftReleased() {
  Serial.println("joy left released");
  switch (joystickMode) {
  case JIRA:
    Keyboard.release('p');
    break;
  case MOUSE:
    xMove = 0;
    break;
  case ARROW:
    Keyboard.release(KEY_LEFT_ARROW);
    break;
  }
}

void joyRightPressed() {
  Serial.println("joy right pressed");
  switch(joystickMode) {
  case JIRA:
    Keyboard.press('n'); // JIRA rapidboard next column
    break;
  case MOUSE:
    xMove = cursorSpeed;
    break;
  case ARROW:
    Keyboard.press(KEY_RIGHT_ARROW);
    break;
  }
}

void joyRightReleased() {
  Serial.println("joy right released");
  switch(joystickMode) {
  case JIRA:
    Keyboard.release('n');
    break;
  case MOUSE:
    xMove = 0;
    break;
  case ARROW:
    Keyboard.release(KEY_RIGHT_ARROW);
    break;
  }
}

void joyUpPressed() {
  Serial.println("joy up pressed");
  switch(joystickMode) {
  case JIRA:
    Keyboard.press('k'); // JIRA rapidboard previous issue
    break;
  case MOUSE:
    yMove = -cursorSpeed;
    break;
  case ARROW:
    Keyboard.press(KEY_UP_ARROW);
    break;
  }
}

void joyUpReleased() {
  Serial.println("joy up released");
  switch(joystickMode) {
  case JIRA:
    Keyboard.release('k');
    break;
  case MOUSE:
    yMove = 0;
    break;
  case ARROW:
    Keyboard.release(KEY_UP_ARROW);
    break;
  }
}

void joyDownPressed() {
  Serial.println("joy down pressed");
  switch(joystickMode) {
  case JIRA:
    Keyboard.press('j'); // JIRA rapidboard next issue
    break;
  case MOUSE:
    yMove = cursorSpeed;
    break;
  case ARROW:
    Keyboard.press(KEY_DOWN_ARROW);
    break;
  }
}

void joyDownReleased() {
  Serial.println("joy down released");
  switch(joystickMode) {
  case JIRA:
    Keyboard.release('j');
    break;
  case MOUSE:
    yMove = 0;
    break;
  case ARROW:
    Keyboard.release(KEY_DOWN_ARROW);
  }
}

void nextPressed() {
  Serial.println("next pressed");
  switch(joystickMode) {
  case JIRA:
    Keyboard.press(KEY_LEFT_GUI); // Should be left CMD key on OSX
    Keyboard.press(KEY_LEFT_ALT); // Left ALT key
    Keyboard.press(KEY_RIGHT_ARROW); // Switch to left tab on Chrome
    break;
  case MOUSE:
    scrollMove = -scrollSpeed;
    break;
  case ARROW:
    Keyboard.press(' ');
    break;
  }
}

void nextReleased() {
  Serial.println("next released");
  switch(joystickMode) {
  case JIRA:
    Keyboard.release(KEY_RIGHT_ARROW);
    Keyboard.release(KEY_LEFT_ALT);
    Keyboard.release(KEY_LEFT_GUI);
    break;
  case MOUSE:
    scrollMove = 0;
    break;
  case ARROW:
    Keyboard.release(' ');
    break;
  }
}

void prevPressed() {
  Serial.println("prev pressed");
  switch(joystickMode) {
  case JIRA:
    Keyboard.press(KEY_LEFT_GUI); // Should be left CMD key on OSX
    Keyboard.press(KEY_LEFT_ALT); // Left ALT key
    Keyboard.press(KEY_LEFT_ARROW); // Switch to left tab on Chrome
    break;
  case MOUSE:
    Mouse.press(MOUSE_LEFT);
    break;
  case ARROW:
    Keyboard.press(' ');
    break;
  }
}

void prevReleased() {
  Serial.println("prev released");
  switch(joystickMode) {
  case JIRA:
    Keyboard.release(KEY_LEFT_ARROW);
    Keyboard.release(KEY_LEFT_ALT);
    Keyboard.release(KEY_LEFT_GUI);
    break;
  case MOUSE:
    Mouse.release(MOUSE_LEFT);
    break;
  case ARROW:
    Keyboard.release(' ');
    break;
  }
}

void reloadPressed() {
  Serial.println("reload pressed");
  switch(joystickMode) {
  case JIRA:
    Keyboard.press(KEY_LEFT_GUI); // Should be left CMD key on OSX
    Keyboard.press('r'); // Reload current tab on Chrome
    break;
  case MOUSE:
    Mouse.press(MOUSE_RIGHT);
    break;
  case ARROW:
    Keyboard.press(' ');
    break;
  }
}

void reloadReleased() {
  Serial.println("relead released");
  switch(joystickMode) {
  case JIRA:
    Keyboard.release('r');
    Keyboard.release(KEY_LEFT_GUI);
    break;
  case MOUSE:
    Mouse.release(MOUSE_RIGHT);
    break;
  case ARROW:
    Keyboard.release(' ');
    break;
  }
}

void detailPressed() {
  Serial.println("detail pressed");
  switch(joystickMode) {
  case JIRA:
    Keyboard.press('t'); // JIRA rapidboard open current issue
    break;
  case MOUSE:
    scrollMove = scrollSpeed;
    break;
  case ARROW:
    Keyboard.press(' ');
    break;
  }
}

void detailReleased() {
  Serial.println("detail released");
  switch(joystickMode) {
  case JIRA:
    Keyboard.release('t');
    break;
  case MOUSE:
    scrollMove = 0;
    break;
  case ARROW:
    Keyboard.release(' ');
    break;
  }
}
