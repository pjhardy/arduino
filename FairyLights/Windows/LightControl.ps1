# Basic control of lights attached to COM6
Param(
  [int]$level
)

$portname = "COM6"
$port = New-Object System.IO.Ports.SerialPort
$port.PortName = $portname
$port.BaudRate = "115200"
$port.Parity = "None"
$port.DataBits = 8
$port.StopBits = 1
$port.ReadTimeout = 9000
$port.DtrEnable = "false"
$port.open()
$port.WriteLine("set {0}" -f $level)
$port.close()

