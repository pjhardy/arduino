// FairyLights
// Leostick driving a set of low-power lights.
// Note that the CmdParser doesn't yet work properly on Leonardo hardware:
// https://github.com/pvizeli/CmdParser/pull/1
#include <CmdBuffer.hpp>
#include <CmdCallback.hpp>
#include <CmdParser.hpp>

const byte lightPin = 6;
CmdCallback<2> cmdCallback;

void setup() {
  pinMode(lightPin, OUTPUT);
  Serial.begin(115200);

  cmdCallback.addCmd("set", &setCallback);
  cmdCallback.addCmd("help", &helpCallback);
}

void loop() {
  CmdBuffer<32> myBuffer;
  CmdParser myParser;
  cmdCallback.loopCmdProcessing(&myParser, &myBuffer, &Serial);
}

void setCallback(CmdParser *myParser) {
  int newLevel = atoi(myParser->getCmdParam(1));
  Serial.print("set ");
  Serial.print(newLevel);
  Serial.println();
  analogWrite(lightPin, newLevel);
  Serial.println("done\n");
}

void helpCallback(CmdParser *myParser) {
  Serial.println("help");
  Serial.print(F("FairyLights!\n============\nSend a command with parameters separated by spaces.\n\nPossible commands:\n'set': Accepts a number between 0-255. Sets the light level.\n'help': Print this help text\n\n"));
}
